from django.contrib import admin
from .models import Student, Event, Course, College, Client, Attendance

# Register your models here.
admin.site.site_header = '2fA Admin Panel'
admin.site.site_title = '2factor Attendance'
admin.site.index_title = '2factor Attendance'


class StudentAdmin(admin.ModelAdmin):
    list_display = ['name', 'course', 'id_number', 'card_uuid']


class EventAdmin(admin.ModelAdmin):
    list_display = ['name', 'start_time', 'end_time']


class CourseAdmin(admin.ModelAdmin):
    list_display = ['name', 'college']


class CollegeAdmin(admin.ModelAdmin):
    list_display = ['name', 'abbreviation']


class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'key']


class AttendanceAdmin(admin.ModelAdmin):
    list_display = ['student', 'event']


admin.site.register(Student, StudentAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(College, CollegeAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Attendance, AttendanceAdmin)

