from django.db import models
from django.utils import timezone
# Create your models here.


class College(models.Model):
    name = models.CharField(max_length=50)
    abbreviation = models.CharField(max_length=5)

    def __str__(self):
        return self.name


class Course(models.Model):
    name = models.CharField(max_length=100)
    abbreviation = models.CharField(max_length=10)
    college = models.ForeignKey(College, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Student(models.Model):
    name = models.CharField(max_length=80)
    id_number = models.CharField(max_length=10, unique=True)
    card_uuid = models.CharField(max_length=25, unique=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(max_length=100)
    venue = models.TextField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

    def __str__(self):
        return '{} - {}'.format(self.name, self.start_time)


class Attendance(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    log_time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '{} in {}'.format(self.student.name, self.event.name)


class Client(models.Model):
    name = models.CharField(max_length=40)
    key = models.CharField(max_length=60)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
