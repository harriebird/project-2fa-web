from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .student import auth_client, register_student, enlist_student
from .models import Course, College, Attendance, Event
from django.core import serializers

def index(request):
    return render(request, 'webapp/index.html')


@csrf_exempt
def register(request):
    if request.method == 'POST':
        client = auth_client(request)
        if client:
            if register_student(request):
                return JsonResponse({'message': 'data received'})
            else:
                return JsonResponse({'message': 'upload failed'}, status=401)
        else:
            return JsonResponse({'message': 'unauthorized'}, status=401)
    else:
        return JsonResponse({'message': 'registrations'})


@csrf_exempt
def present_student(request):
    if request.method == 'POST':
        client = auth_client(request)
        if client:
            if enlist_student(request, client):
                return JsonResponse({'message': 'data received'})
            else:
                return JsonResponse({'message': 'upload failed'}, status=401)
        else:
            return JsonResponse({'message': 'unauthorized'}, status=401)
    else:
        return JsonResponse({'message': 'attendance'})


def get_colleges(request):
    colleges = College.objects.all().order_by('name')
    colleges_json = serializers.serialize('json', colleges)
    return HttpResponse(colleges_json, content_type='application/json')


def get_courses(request, pk):
    courses = Course.objects.filter(college_id=pk).order_by('name')
    courses_json = serializers.serialize('json', courses)
    return HttpResponse(courses_json, content_type='application/json')


def list_present(request):
    events = Event.objects.all().order_by('-start_time')

    event_id = request.GET.get('event_id', '')
    if event_id and event_id.isdigit():
        present = Attendance.objects.filter(event_id=event_id).order_by('student__name')
        data = {'events': events, 'present': present}
    else:
        data = {'events': events}

    return render(request, 'webapp/list.html', {'data': data})
