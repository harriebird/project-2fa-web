from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('register', views.register, name='register'),
    path('colleges', views.get_colleges, name='get_colleges'),
    path('courses/<int:pk>', views.get_courses, name='get_courses'),
    path('attendance', views.present_student, name='get_courses'),
    path('list', views.list_present, name='list_present')
    # path('bot_data/<int:bot_id>', views.get_data, name='bot_data'),
    # path('bot_data/<int:bot_id>/<str:req_date>', views.get_data, name='bot_data')
]
