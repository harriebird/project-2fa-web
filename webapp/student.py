from .models import Client, Student, Attendance
from django.core.exceptions import ObjectDoesNotExist
import csv
from django.db.models import Avg
import os
from django.conf import settings
from django.utils import timezone
import datetime


def auth_client(request):
    name = request.POST.get('client_name', '')
    key = request.POST.get('client_key', '')

    try:
        client = Client.objects.get(name=name, key=key)
    except ObjectDoesNotExist:
        return False

    return client


def register_student(request):
    name = request.POST.get('name', '')
    id_number = request.POST.get('id_number', '')
    card_uuid = request.POST.get('card_uuid', '')
    course_id = request.POST.get('course_id', '')

    student = Student(name=name, id_number=id_number, card_uuid=card_uuid, course_id=course_id)
    student.save()
    return True


def enlist_student(request, client):
    id_number = request.POST.get('id_number', '')
    card_uuid = request.POST.get('card_uuid', '')
    try:
        student = Student.objects.get(id_number=id_number, card_uuid=card_uuid)
    except ObjectDoesNotExist:
        return False

    count = Attendance.objects.filter(student=student, event=client.event).count()
    if count < 1:
        attendance = Attendance(student=student, event=client.event)
        attendance.save()
        return True
    else:
        return False


